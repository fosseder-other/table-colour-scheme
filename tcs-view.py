#!/bin/python

import sys

def hex_to_ansi(hex_code):
    # Remove '#' if present
    hex_code = hex_code.lstrip('#')

    # Convert HEX to RGB
    red = int(hex_code[0:2], 16)
    green = int(hex_code[2:4], 16)
    blue = int(hex_code[4:6], 16)

    # Convert RGB to ANSI escape sequence
    ansi_escape = f"\033[38;2;{red};{green};{blue}m"
    return ansi_escape

def reset_ansi():
    return f"\033[0m"

def read_tcs():
    # Read lines from stdin
    value = []
    data = []
    raw = []
    for line in sys.stdin:
        line = line.strip()
        if line:
            # Split the line at the first comma
            index = line.find(",")
            if index != -1:
                hex_value = line[:index].strip().upper()
                text = line[index+1:].strip()
                row = [hex_to_ansi(hex_value) + text, hex_value]
                value.append(hex_value)
                data.append(row)
                raw.append(text)
    return value, data, raw

def main():
    value, data, raw = read_tcs()

    max_width = max(len(text) for text in raw)

    for index, row in enumerate(data):
        padding = max_width - len(raw[index])
        print(f"{row[0]}{' ' * padding}{reset_ansi()}\t{hex_to_ansi(value[index])}{row[1]}{reset_ansi()}")

main()
