# Table Colour Scheme
Table Colour Scheme (shortened "TCS") is a way to display colour schemes in a human-readable and easily machine-parsed way.

## Installation

Make sure that `~/.local/bin` is included in your `$PATH`:
```bash
echo $PATH | grep '/.local/bin'
```
**If it isn't**, then add this to your .bashrc (*search up how to do it in your shell if you don't use bash*):
```bash
export PATH="$HOME/.local/bin:$PATH"
```
Then, run the following:
```bash
cd $HOME/.local/bin/
wget 'https://gitlab.com/fosseder-other/table-colour-scheme/-/raw/main/tcs-view.py'
chmod u+x tcs-view.py
```
And you're done! 
## Example Usage

Simple pipe the content of TCS files into the python file.

![cat catppuccin-macchiato.tcs | tcs-view.py](usage.png "cat catppuccin-macchiato.tcs | tcs-view.py")

### Example TCS files

See [examples](examples/).

## LICENCE

[GNU General Public License v3](LICENCE)
